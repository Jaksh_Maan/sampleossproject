# Contributing

1.  Create a fork of the project
1.  Clone your fork to your development environment
1.  Open an issue describing the change that you wish to make
1.  Create a branch off of master for your changes
1.  Make your changes and commits
    * Reference the issue number in commit subject
1.  Push your branch to your fork
1.  Open a merge request referencing your issue number
1.  Check back for any requested changes
